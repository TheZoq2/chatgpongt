# A pong game in Rust generated entirely by ChatGPT

> Write a pong game in rust using macroquad

Creating a Pong game in Rust using the macroquad library involves several steps. Make sure you have Rust and Cargo installed on your system. You can install macroquad by adding it to your Cargo.toml file:

```toml
[dependencies]
macroquad = "0.10.2"
```

Now, create a new Rust file (e.g., main.rs) and use the following code as a starting point:

```rust
use macroquad::prelude::*;

const PADDLE_SPEED: f32 = 300.0;
const PADDLE_HEIGHT: f32 = 80.0;
const PADDLE_WIDTH: f32 = 10.0;
const BALL_SPEED: f32 = 200.0;
const BALL_SIZE: f32 = 10.0;

struct Paddle {
    x: f32,
    y: f32,
    height: f32,
}

struct Ball {
    x: f32,
    y: f32,
    speed_x: f32,
    speed_y: f32,
}

struct GameState {
    left_paddle: Paddle,
    right_paddle: Paddle,
    ball: Ball,
}

#[macroquad::main("Pong")]
async fn main() {
    let mut game_state = GameState {
        left_paddle: Paddle {
            x: 20.0,
            y: screen_height() / 2.0 - PADDLE_HEIGHT / 2.0,
            height: PADDLE_HEIGHT,
        },
        right_paddle: Paddle {
            x: screen_width() - 30.0,
            y: screen_height() / 2.0 - PADDLE_HEIGHT / 2.0,
            height: PADDLE_HEIGHT,
        },
        ball: Ball {
            x: screen_width() / 2.0 - BALL_SIZE / 2.0,
            y: screen_height() / 2.0 - BALL_SIZE / 2.0,
            speed_x: BALL_SPEED,
            speed_y: BALL_SPEED,
        },
    };

    loop {
        clear_background(BLACK);

        // Update game logic
        update_game(&mut game_state);

        // Draw game elements
        draw_paddle(game_state.left_paddle.x, game_state.left_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
        draw_paddle(game_state.right_paddle.x, game_state.right_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
        draw_rectangle(game_state.ball.x, game_state.ball.y, BALL_SIZE, BALL_SIZE, WHITE);

        next_frame().await
    }
}

fn update_game(state: &mut GameState) {
    // Update paddle positions
    handle_input(&mut state.left_paddle);
    // Add AI or player input for the right paddle

    // Update ball position
    state.ball.x += state.ball.speed_x * get_frame_time();
    state.ball.y += state.ball.speed_y * get_frame_time();

    // Bounce off walls
    if state.ball.y <= 0.0 || state.ball.y >= screen_height() - BALL_SIZE {
        state.ball.speed_y = -state.ball.speed_y;
    }

    // Check for paddle collisions
    if check_collision_paddle(state.ball, state.left_paddle)
        || check_collision_paddle(state.ball, state.right_paddle)
    {
        state.ball.speed_x = -state.ball.speed_x;
    }

    // Check for scoring
    if state.ball.x < 0.0 || state.ball.x > screen_width() {
        // Reset ball position
        state.ball.x = screen_width() / 2.0 - BALL_SIZE / 2.0;
        state.ball.y = screen_height() / 2.0 - BALL_SIZE / 2.0;
        // Reset ball speed and direction
        state.ball.speed_x = BALL_SPEED;
        state.ball.speed_y = BALL_SPEED;
    }
}

fn handle_input(paddle: &mut Paddle) {
    // Move paddle up
    if is_key_down(KeyCode::W) && paddle.y > 0.0 {
        paddle.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move paddle down
    if is_key_down(KeyCode::S)
        && paddle.y + paddle.height < screen_height()
    {
        paddle.y += PADDLE_SPEED * get_frame_time();
    }
}

fn draw_paddle(x: f32, y: f32, width: f32, height: f32) {
    draw_rectangle(x, y, width, height, WHITE);
}

fn check_collision_paddle(ball: Ball, paddle: Paddle) -> bool {
    let ball_right = ball.x + BALL_SIZE;
    let ball_bottom = ball.y + BALL_SIZE;
    let paddle_right = paddle.x + PADDLE_WIDTH;
    let paddle_bottom = paddle.y + paddle.height;

    if ball_right >= paddle.x
        && ball.x <= paddle_right
        && ball_bottom >= paddle.y
        && ball.y <= paddle_bottom
    {
        return true;
    }

    false
}
```

This is a basic Pong game using the macroquad library. You can customize and extend it according to your needs. Note that this example does not include player input for the right paddle or any form of AI. You may want to implement those features based on your requirements.

> Could you add the player input too? With w and s as the up and down keys?

Certainly! I've added player input for the right paddle using the W and S keys. Here's the modified handle_input function:

```rust
fn handle_input(paddle_left: &mut Paddle, paddle_right: &mut Paddle) {
    // Move left paddle up
    if is_key_down(KeyCode::W) && paddle_left.y > 0.0 {
        paddle_left.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move left paddle down
    if is_key_down(KeyCode::S) && paddle_left.y + paddle_left.height < screen_height() {
        paddle_left.y += PADDLE_SPEED * get_frame_time();
    }

    // Move right paddle up
    if is_key_down(KeyCode::Up) && paddle_right.y > 0.0 {
        paddle_right.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move right paddle down
    if is_key_down(KeyCode::Down) && paddle_right.y + paddle_right.height < screen_height() {
        paddle_right.y += PADDLE_SPEED * get_frame_time();
    }
}
```

I've modified the handle_input function to take both left and right paddles as arguments. Now, the W and S keys control the left paddle, and the Up and Down arrow keys control the right paddle.

You should also update the main function to pass both paddles to the handle_input function:

```rust
loop {
    clear_background(BLACK);

    // Update game logic
    handle_input(&mut game_state.left_paddle, &mut game_state.right_paddle);
    update_game(&mut game_state);

    // Draw game elements
    draw_paddle(game_state.left_paddle.x, game_state.left_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
    draw_paddle(game_state.right_paddle.x, game_state.right_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
    draw_rectangle(game_state.ball.x, game_state.ball.y, BALL_SIZE, BALL_SIZE, WHITE);

    next_frame().await
}
```

Now, both paddles should respond to player input, with the left paddle controlled by the W and S keys and the right paddle controlled by the Up and Down arrow keys.


> The latest version of macroquad is 0.4.0

Certainly, I'll update the code to use version 0.4.0 of macroquad. Please note that there might be breaking changes between different versions of the library, so it's a good practice to check the documentation for the specific version you are using.

First, update your Cargo.toml file:

```toml

[dependencies]
macroquad = "0.4.0"
```

Now, update the code:

```rust

use macroquad::prelude::*;

const PADDLE_SPEED: f32 = 300.0;
const PADDLE_HEIGHT: f32 = 80.0;
const PADDLE_WIDTH: f32 = 10.0;
const BALL_SPEED: f32 = 200.0;
const BALL_SIZE: f32 = 10.0;

struct Paddle {
    x: f32,
    y: f32,
    height: f32,
}

struct Ball {
    x: f32,
    y: f32,
    speed_x: f32,
    speed_y: f32,
}

struct GameState {
    left_paddle: Paddle,
    right_paddle: Paddle,
    ball: Ball,
}

#[macroquad::main("Pong")]
async fn main() {
    let mut game_state = GameState {
        left_paddle: Paddle {
            x: 20.0,
            y: screen_height() / 2.0 - PADDLE_HEIGHT / 2.0,
            height: PADDLE_HEIGHT,
        },
        right_paddle: Paddle {
            x: screen_width() - 30.0,
            y: screen_height() / 2.0 - PADDLE_HEIGHT / 2.0,
            height: PADDLE_HEIGHT,
        },
        ball: Ball {
            x: screen_width() / 2.0 - BALL_SIZE / 2.0,
            y: screen_height() / 2.0 - BALL_SIZE / 2.0,
            speed_x: BALL_SPEED,
            speed_y: BALL_SPEED,
        },
    };

    loop {
        clear_background(BLACK);

        // Update game logic
        handle_input(&mut game_state.left_paddle, &mut game_state.right_paddle);
        update_game(&mut game_state);

        // Draw game elements
        draw_paddle(game_state.left_paddle.x, game_state.left_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
        draw_paddle(game_state.right_paddle.x, game_state.right_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
        draw_rectangle(game_state.ball.x, game_state.ball.y, BALL_SIZE, BALL_SIZE, WHITE);

        next_frame().await
    }
}

fn update_game(state: &mut GameState) {
    // Update paddle positions
    // No changes needed here

    // Update ball position
    state.ball.x += state.ball.speed_x * get_frame_time();
    state.ball.y += state.ball.speed_y * get_frame_time();

    // Bounce off walls
    if state.ball.y <= 0.0 || state.ball.y >= screen_height() - BALL_SIZE {
        state.ball.speed_y = -state.ball.speed_y;
    }

    // Check for paddle collisions
    if check_collision_paddle(state.ball, state.left_paddle)
        || check_collision_paddle(state.ball, state.right_paddle)
    {
        state.ball.speed_x = -state.ball.speed_x;
    }

    // Check for scoring
    if state.ball.x < 0.0 || state.ball.x > screen_width() {
        // Reset ball position
        state.ball.x = screen_width() / 2.0 - BALL_SIZE / 2.0;
        state.ball.y = screen_height() / 2.0 - BALL_SIZE / 2.0;
        // Reset ball speed and direction
        state.ball.speed_x = BALL_SPEED;
        state.ball.speed_y = BALL_SPEED;
    }
}

fn handle_input(paddle_left: &mut Paddle, paddle_right: &mut Paddle) {
    // Move left paddle up
    if is_key_down(KeyCode::W) && paddle_left.y > 0.0 {
        paddle_left.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move left paddle down
    if is_key_down(KeyCode::S) && paddle_left.y + paddle_left.height < screen_height() {
        paddle_left.y += PADDLE_SPEED * get_frame_time();
    }

    // Move right paddle up
    if is_key_down(KeyCode::Up) && paddle_right.y > 0.0 {
        paddle_right.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move right paddle down
    if is_key_down(KeyCode::Down) && paddle_right.y + paddle_right.height < screen_height() {
        paddle_right.y += PADDLE_SPEED * get_frame_time();
    }
}

fn draw_paddle(x: f32, y: f32, width: f32, height: f32) {
    draw_rectangle(x, y, width, height, WHITE);
}

fn check_collision_paddle(ball: Ball, paddle: Paddle) -> bool {
    let ball_right = ball.x + BALL_SIZE;
    let ball_bottom = ball.y + BALL_SIZE;
    let paddle_right = paddle.x + PADDLE_WIDTH;
    let paddle_bottom = paddle.y + paddle.height;

    if ball_right >= paddle.x
        && ball.x <= paddle_right
        && ball_bottom >= paddle.y
        && ball.y <= paddle_bottom
    {
        return true;
    }

    false
}
```

This code should work with macroquad version 0.4.0. Make sure to check the official documentation for any additional changes or updates related to the library version you are using.


> it gives me some error messages:
> 
>   --> src/main.rs:79:31
>    |
> 79 |     if check_collision_paddle(state.ball, state.left_paddle)
>    |                               ^^^^^^^^^^ move occurs because `state.ball` has type `Ball`, which does not implement the `Copy` trait
> 
> error[E0507]: cannot move out of `state.left_paddle` which is behind a mutable reference
>   --> src/main.rs:79:43
>    |
> 79 |     if check_collision_paddle(state.ball, state.left_paddle)
>    |                                           ^^^^^^^^^^^^^^^^^ move occurs because `state.left_paddle` has type `Paddle`, which does not implement the `Copy` trait
> 
> error[E0507]: cannot move out of `state.ball` which is behind a mutable reference
>   --> src/main.rs:80:35
>    |
> 80 |         || check_collision_paddle(state.ball, state.right_paddle)
>    |                                   ^^^^^^^^^^ move occurs because `state.ball` has type `Ball`, which does not implement the `Copy` trait
> 
> error[E0507]: cannot move out of `state.right_paddle` which is behind a mutable reference
>   --> src/main.rs:80:47
>    |
> 80 |         || check_collision_paddle(state.ball, state.right_paddle)
>    |                                               ^^^^^^^^^^^^^^^^^^ move occurs because `state.right_paddle` has type `Paddle`, which does not implement the `Copy` trait
> 
> For more information about this error, try `rustc --explain E0507`.
> error: could not compile `aipong` (bin "aipong") due to 4 previous errors
> ChatGPT

The error messages indicate that there are issues with moving out of types that do not implement the Copy trait. To fix this, you can use references instead of moving the values. Here's the modified update_game function:

```rust

fn update_game(state: &mut GameState) {
    // Update paddle positions
    // No changes needed here

    // Update ball position
    state.ball.x += state.ball.speed_x * get_frame_time();
    state.ball.y += state.ball.speed_y * get_frame_time();

    // Bounce off walls
    if state.ball.y <= 0.0 || state.ball.y >= screen_height() - BALL_SIZE {
        state.ball.speed_y = -state.ball.speed_y;
    }

    // Check for paddle collisions
    if check_collision_paddle(&state.ball, &state.left_paddle)
        || check_collision_paddle(&state.ball, &state.right_paddle)
    {
        state.ball.speed_x = -state.ball.speed_x;
    }

    // Check for scoring
    if state.ball.x < 0.0 || state.ball.x > screen_width() {
        // Reset ball position
        state.ball.x = screen_width() / 2.0 - BALL_SIZE / 2.0;
        state.ball.y = screen_height() / 2.0 - BALL_SIZE / 2.0;
        // Reset ball speed and direction
        state.ball.speed_x = BALL_SPEED;
        state.ball.speed_y = BALL_SPEED;
    }
}

And also update the check_collision_paddle function to take references:

rust

fn check_collision_paddle(ball: &Ball, paddle: &Paddle) -> bool {
    let ball_right = ball.x + BALL_SIZE;
    let ball_bottom = ball.y + BALL_SIZE;
    let paddle_right = paddle.x + PADDLE_WIDTH;
    let paddle_bottom = paddle.y + paddle.height;

    if ball_right >= paddle.x
        && ball.x <= paddle_right
        && ball_bottom >= paddle.y
        && ball.y <= paddle_bottom
    {
        return true;
    }

    false
}
```

By passing references to the functions instead of moving values, you avoid the ownership issues. This should resolve the compilation errors you're facing.


> Could I also get some AI?

Certainly! Implementing AI for the right paddle in Pong involves making the paddle follow the ball's vertical position. Here's an example of a simple AI for the right paddle:

```rust

fn update_game(state: &mut GameState) {
    // ... (previous code remains unchanged)

    // AI for the right paddle
    let ai_speed = PADDLE_SPEED * get_frame_time();
    if state.right_paddle.y + state.right_paddle.height / 2.0 < state.ball.y {
        // Move paddle down
        state.right_paddle.y += ai_speed;
    } else {
        // Move paddle up
        state.right_paddle.y -= ai_speed;
    }
}
```

This code checks if the ball is above or below the center of the right paddle and adjusts the paddle's position accordingly. The ai_speed variable ensures that the AI paddle's movement is frame rate independent.

You can add this AI logic to the update_game function. Now, both paddles should respond to player input, with the left paddle controlled by the W and S keys, and the right paddle controlled by a simple AI that follows the ball's vertical position. Adjustments can be made to the AI logic for more sophisticated behavior based on your preferences.

