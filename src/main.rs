use macroquad::prelude::*;

const PADDLE_SPEED: f32 = 300.0;
const PADDLE_HEIGHT: f32 = 80.0;
const PADDLE_WIDTH: f32 = 10.0;
const BALL_SPEED: f32 = 200.0;
const BALL_SIZE: f32 = 10.0;

struct Paddle {
    x: f32,
    y: f32,
    height: f32,
}

struct Ball {
    x: f32,
    y: f32,
    speed_x: f32,
    speed_y: f32,
}

struct GameState {
    left_paddle: Paddle,
    right_paddle: Paddle,
    ball: Ball,
}

#[macroquad::main("Pong")]
async fn main() {
    let mut game_state = GameState {
        left_paddle: Paddle {
            x: 20.0,
            y: screen_height() / 2.0 - PADDLE_HEIGHT / 2.0,
            height: PADDLE_HEIGHT,
        },
        right_paddle: Paddle {
            x: screen_width() - 30.0,
            y: screen_height() / 2.0 - PADDLE_HEIGHT / 2.0,
            height: PADDLE_HEIGHT,
        },
        ball: Ball {
            x: screen_width() / 2.0 - BALL_SIZE / 2.0,
            y: screen_height() / 2.0 - BALL_SIZE / 2.0,
            speed_x: BALL_SPEED,
            speed_y: BALL_SPEED,
        },
    };

    loop {
        clear_background(BLACK);

        // Update game logic
        handle_input(&mut game_state.left_paddle, &mut game_state.right_paddle);
        update_game(&mut game_state);

        // Draw game elements
        draw_paddle(game_state.left_paddle.x, game_state.left_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
        draw_paddle(game_state.right_paddle.x, game_state.right_paddle.y, PADDLE_WIDTH, PADDLE_HEIGHT);
        draw_rectangle(game_state.ball.x, game_state.ball.y, BALL_SIZE, BALL_SIZE, WHITE);

        next_frame().await
    }
}

fn update_game(state: &mut GameState) {
    // Update paddle positions
    // No changes needed here

    // Update ball position
    state.ball.x += state.ball.speed_x * get_frame_time();
    state.ball.y += state.ball.speed_y * get_frame_time();

    // Bounce off walls
    if state.ball.y <= 0.0 || state.ball.y >= screen_height() - BALL_SIZE {
        state.ball.speed_y = -state.ball.speed_y;
    }

    // Check for paddle collisions
    if check_collision_paddle(&state.ball, &state.left_paddle)
        || check_collision_paddle(&state.ball, &state.right_paddle)
    {
        state.ball.speed_x = -state.ball.speed_x;
    }

    // Check for scoring
    if state.ball.x < 0.0 || state.ball.x > screen_width() {
        // Reset ball position
        state.ball.x = screen_width() / 2.0 - BALL_SIZE / 2.0;
        state.ball.y = screen_height() / 2.0 - BALL_SIZE / 2.0;
        // Reset ball speed and direction
        state.ball.speed_x = BALL_SPEED;
        state.ball.speed_y = BALL_SPEED;
    }

    // AI for the right paddle
    let ai_speed = PADDLE_SPEED * get_frame_time();
    if state.right_paddle.y + state.right_paddle.height / 2.0 < state.ball.y {
        // Move paddle down
        state.right_paddle.y += ai_speed;
    } else {
        // Move paddle up
        state.right_paddle.y -= ai_speed;
    }
}

fn handle_input(paddle_left: &mut Paddle, paddle_right: &mut Paddle) {
    // Move left paddle up
    if is_key_down(KeyCode::W) && paddle_left.y > 0.0 {
        paddle_left.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move left paddle down
    if is_key_down(KeyCode::S) && paddle_left.y + paddle_left.height < screen_height() {
        paddle_left.y += PADDLE_SPEED * get_frame_time();
    }

    // Move right paddle up
    if is_key_down(KeyCode::Up) && paddle_right.y > 0.0 {
        paddle_right.y -= PADDLE_SPEED * get_frame_time();
    }

    // Move right paddle down
    if is_key_down(KeyCode::Down) && paddle_right.y + paddle_right.height < screen_height() {
        paddle_right.y += PADDLE_SPEED * get_frame_time();
    }
}

fn draw_paddle(x: f32, y: f32, width: f32, height: f32) {
    draw_rectangle(x, y, width, height, WHITE);
}

fn check_collision_paddle(ball: &Ball, paddle: &Paddle) -> bool {
    let ball_right = ball.x + BALL_SIZE;
    let ball_bottom = ball.y + BALL_SIZE;
    let paddle_right = paddle.x + PADDLE_WIDTH;
    let paddle_bottom = paddle.y + paddle.height;

    if ball_right >= paddle.x
        && ball.x <= paddle_right
        && ball_bottom >= paddle.y
        && ball.y <= paddle_bottom
    {
        return true;
    }

    false
}
